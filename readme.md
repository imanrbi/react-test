## 1) Install Package
```
npm install
```
You have all you need in ```package.json``` file

## 2) Make Routes
Make the following routes

| Route                | Description        | Component |
| -------------------- | ------------------ | --------  |
| /                    | Show all articles  | Articles  |
| /article/{articleId} | Show article by id | Article   |

## 3) Get Data From ```db.json``` File
Get data from ```db.json``` file and decorate articles with existing objects in file, then save decorated articles object in redux store
###
example:
```
                             db.json
    +----------+ +------------+ +---------------------+ +-------+
    |          | |            | |                     | |       |
    | articles | | categories | | articles_categoreis | | users |
    |          | |            | |                     | |       |
    +----------+ +------------+ +---------------------+ +-------+

                                ||
                                ||
                                \/
                         
                  decorate object in redux store
            +----------------------------------------+
            |articles: [                             |
            | {                                      |
            |   id: 1,                               |
            |   title: "Article 1",                  |
            |   description: "Article 1 Description",|
            |   image: "https://picsum.photos/200",  |
            |   categories: ["react","ux"],          |
            |   author: "User 1",                    |
            |   content: "...",                      |
            | },                                     |
            | ...                                    |
            |]                                       |
            +----------------------------------------+                
```
## 4) Display articles with ```ArticleCard``` Component
after save articles object in redux store , display articles with ```ArticleCard``` component in ```Articles``` component
```
            +----------------------------------------+
            | Articles Component                     |
            | +------------------------------------+ |
            | |                                    | |
            | |       ArticleCard Component        | |
            | |                                    | |
            | +------------------------------------+ |
            | +------------------------------------+ |
            | |                                    | |
            | |       ArticleCard Component        | |
            | |                                    | |
            | +------------------------------------+ |
            |                  ....                  |
            +----------------------------------------+
```

## 5) Article Page
in route ```/``` when click more button,get article object by id from redux store and display article in ```Article``` component