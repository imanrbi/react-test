import React from 'react';
import { Card, Col, Button } from 'react-bootstrap';

function ArticleCard(props:any) {

  const {id, title, image, description, author, categories } = props;

  return (
    <Col md={3} className={'pb-4'}>
      <Card>
        <Card.Img variant="top" src={image} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>
            {description}
          </Card.Text>
          <Card.Text>
           <span>Author: </span> {author}
          </Card.Text>
          <Card.Text>
            <span>Categories: </span> {categories.map((category:string,index:number) => category  + ((index + 1 < categories.length) ? ', ':''))}
          </Card.Text>
          <Button variant="secondary" href={'artcile/'+id} size={'sm'}>More</Button>
        </Card.Body>
      </Card>
    </Col>
  );
}

export default ArticleCard;
