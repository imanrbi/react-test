import React from 'react';
import { Row, Col, Image, Button } from 'react-bootstrap';

function Article(props: any) {

  const { id, title, image, description, author, categories, content } = props;

  return (
    <Row>
      <Col xs={12} className={'pt-3'}>
        <Button href="/" variant={'secondary'}>Back</Button>
      </Col>
      <Col xs={12} className={'pt-3'}>
        <Image src={image} fluid rounded />
      </Col>
      <Col md={8} className={'pt-3'}>
        <h2>{title}</h2>
        <h6>{description}</h6>
      </Col>
      <Col md={4} className={'pt-4'}>
        <h6 className={'mb-2'}><span className={'font-weight-bold'}>Author: </span>{author}</h6>
        <h6><span className={'font-weight-bold'}>Categories: </span>{categories.map((category: string, index: number) => category + ((index + 1 < categories.length) ? ', ' : ''))}</h6>
      </Col>
      <Col xs={12}>
        <hr />
        <p>
          {content}
        </p>
      </Col>
    </Row>
  );
}

export default Article;
